<?php

declare(strict_types=1);

namespace RodgauerWorkshop\ContaoCivicrmBundle\Controller\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Contao\Template;
use Contao\FilesModel;
use Contao\Controller;
use Contao\System;
use Contao\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use RodgauerWorkshop\ContaoCivicrmBundle\CivicrmHelper;

/**
 * @ContentElement("datawriter",
 *   category="RW", 
 *   template="ce_datawriter"
 * )
 */
class DataWriterElement extends AbstractContentElementController
{
    use CivicrmHelper;

    protected function getResponse(Template $template, ContentModel $model, Request $request): ?Response
    {
        if (TL_MODE == 'BE') {
            $template                 = new \BackendTemplate('be_datawriter');
            $template->label_db       = $GLOBALS['TL_LANG']['tl_content']['db_table'][0];
            $template->label_workshop = $GLOBALS['TL_LANG']['tl_content']['workshop'][0];
            $template->db_table       = $model->db_table;
            $template->workshop       = $model->workshop;
        } else {
            if (isset($_GET['id'])) { $id = (int) $_GET['id']; }
            if ($id > 0) {
                $db = Database::getInstance();
                $temp = $model->output;
                $contact_id = 0;
                $user = array();

                $part = $this->getParticipant(array('id'=>$id));
                
                if ($part['is_error'] == 0 && $part['count'] == 1) {
                    $contact_id = $part['values'][$id]['contact_id'];

                    $user = $this->getContact(array('id'=>$contact_id));

                    if ($user['is_error'] == 0 && $user['count'] == 1) {
                        $temp = str_replace('{{first_name}}', $user['values'][$contact_id]['first_name'], $temp);
                        $temp = str_replace('{{last_name}}', $user['values'][$contact_id]['last_name'], $temp);
                        $temp = str_replace('{{sort_name}}', $user['values'][$contact_id]['sort_name'], $temp);
                        $temp = str_replace('{{display_name}}', $user['values'][$contact_id]['display_name'], $temp);
                        $temp = str_replace('{{email}}', $user['values'][$contact_id]['email'], $temp);

                        $name = $user['values'][$contact_id]['sort_name'];

                        $entry = $db->prepare("SELECT id, user, status FROM ".$model->db_table." WHERE id=? and workshop=?")
                                    ->execute($id, $model->workshop);
        
                        if ($entry->numRows <> 0) {
                          $db->prepare("UPDATE ".$model->db_table." SET status='OK', user=?, message='datawriter' WHERE id=? and workshop=?")
                             ->execute($name, $id, $model->workshop);
                        } else {
                          $db->prepare("INSERT INTO ".$model->db_table." SET id=?, status='OK', user=?, workshop=?, message='datawriter'")
                             ->execute($id, $name, $model->workshop);
                        }
        
                        mail($model->mail, "[RW] OK: ".$name." - ".$model->workshop, "OK!");

                        $template->content = $temp;
                    } else {
                        $template->content = "Es ist ein Fehler aufgetreten!";
                    }
                } else {
                    $template->content = "Es ist ein Fehler aufgetreten!";
                }
            } else {
                $template->content = "Es ist ein Fehler aufgetreten!";
            }
        }
        return $template->getResponse();
    }
}