<?php

declare(strict_types=1);

/*
 * This file is part of [package name].
 *
 * (c) John Doe
 *
 * @license LGPL-3.0-or-later
 */

namespace RodgauerWorkshop\ContaoCivicrmBundle\ContaoManager;

use Contao\NewsletterBundle\ContaoCoreBundle;
use Contao\NewsletterBundle\ContaoNewsletterBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use RodgauerWorkshop\ContaoCivicrmBundle\ContaoCivicrmBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ContaoCivicrmBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class, ContaoNewsletterBundle::class]),
        ];
    }
}
