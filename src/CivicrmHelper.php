<?php

declare(strict_types=1);

namespace RodgauerWorkshop\ContaoCivicrmBundle;

use Contao\Database;
use GuzzleHttp\Client;

trait CivicrmHelper
{
	/* map Contao fields to CiviCRM fields */
	private $arrMapping = array(
		'username'=>'nick_name',
		'email'=>'email',
		'website'=>'home_URL',
		'firstname'=>'first_name',
		'lastname'=>'last_name',
		);

	private $Database;


	public function updateNewsletter($varValue, $objUser, $force = false)
	/* TODO: broken with API V4 */
	{
		/* early return until it is fixed */
		return null;
		
		$db = Database::getInstance();
		
		// If called from the back end, the second argument is a DataContainer object
		if ($objUser instanceof DataContainer)
		{
			$objUser = $db->prepare("SELECT * FROM tl_member WHERE id=?")
						  ->limit(1)
						  ->execute($objUser->id);

			if ($objUser->numRows < 1)
			{
				return $varValue;
			}
		}

		// Nothing has changed or member not tracked in CiviCRM
		if (($varValue == $objUser->newsletter && !$force) || !$objUser->civicrm_id)
		{
			return $varValue;
		}

		$varValue = deserialize($varValue, true);

		// Get all channel IDs
		$objChannel = $db->execute("SELECT id FROM tl_newsletter_channel");
		$arrChannel = $objChannel->fetchEach('id');
		$arrOld = deserialize($objUser->newsletter, true);

		$arrDelete = array_values(array_diff($arrOld, $varValue));

		if (count($arrDelete) > 0)
		{
			foreach ($arrDelete as $intChannel)
			{
				$arrGroups = $db->prepare("SELECT civicrm_group FROM tl_newsletter_channel WHERE id=? AND use_civicrm=1")
								->execute($intChannel)
								->fetchEach('civicrm_group');
				if (count($arrGroups)>0) { $this->removeFromGroup($objUser->civicrm_id, array_values($arrGroups)[0]); }
			}
		}

		/* add subscribtions add civicrm */
		if (count($varValue) > 0)
		{
			foreach ($varValue as $intChannel)
			{
				$arrGroups = $db->prepare("SELECT civicrm_group FROM tl_newsletter_channel WHERE id=? AND use_civicrm=1")
								->execute($intChannel)
								->fetchEach('civicrm_group');
				if (count($arrGroups)>0) { $this->addToGroup($objUser->civicrm_id, array_values($arrGroups)[0]); }
			}
		}

		return null;
	}


	public function updateEmail($varValue, $objUser)
	/* TODO: broken with API V4 */
	{
		/* early exit until it is fixed */
		return null;

		$db = Database::getInstance();
		// If called from the back end, the second argument is a DataContainer object
		if ($objUser instanceof DataContainer)
		{
			$objUser = $db->prepare("SELECT * FROM tl_member WHERE id=?")
						  ->limit(1)
						  ->execute($objUser->id);

			if ($objUser->numRows < 1)
			{
				return $varValue;
			}
		}

		if ($objUser->civicrm_id)
		{
			$arrData = array('contact_id'=>$objUser->civicrm_id,
							 'contact_type'=>'Individual',
							 'email[1][email]'=>$varValue,
							 'email[1][is_primary]'=>1,
							 'email[1][location_type_id]'=>1);
			$arrUrlParams = $this->buildUrl($arrData);
			$arrResult = $this->writeToUrl('Contact/create', $arrUrlParams);
		}
		return $varValue;
	}


	public function getAllGroups()
	{
		$return = array();

		if ($GLOBALS['TL_CONFIG']['civicrm_apiurl'] && $GLOBALS['TL_CONFIG']['civicrm_sitekey'] && $GLOBALS['TL_CONFIG']['civicrm_apikey'])
		{
			$arrUrlParams = $this->buildUrl();
			$arrGroups = $this->readFromUrl('Group/get', $arrUrlParams);

			$return = array();
			foreach ($arrGroups['values'] as $group) {
				$return[$group['id']]=$group['title'];
				asort($return);
			}
		} else {
			$return = false;
		}

		return $return;
	}

	/*
	 * Private Functions
	 *
	 */


	/*
	 * Contacts
	 */

	protected function getContact($arrSearch)
	/* TODO: broken with API V4 */
	{
		$arrUrlParams = $this->buildUrl($arrSearch);
		$arrReturn = $this->readFromUrl('Contact/get', $arrUrlParams);

		return $arrReturn;
	}

	protected function getContactByMail($strEmail)
	{
		$strQuery = '{"select":["*", "contact_id.*"],"where":[["email","=","'.$strEmail.'"]]}';
		$arrUrlParams = $this->buildUrl(['params'=>$strQuery]);

		$arrReturn = $this->readFromUrl('Email/get', $arrUrlParams);

		return $arrReturn;
	}


	protected function createContact($objUser)
	/* TODO: broken with API V4 */
	{
		$arrData = array('"contact_type":"Individual"');


		foreach ($this->arrMapping as $tl => $civi)
		{
			if ($objUser->$tl)
			{
				$arrData[]='"'.$civi.'":"'.$tl.'"';
			}
		}

		$strParams = '{values:{'.implode(",", $arrData).'}';

		/* Anlegen */
		$arrUrlParams = $this->buildUrl(["params"=>$strParams]);
		$arrResult = $this->writeToUrl('Contact/create', $arrUrlParams);

		$valReturn = ($arrResult['count']==1) ? $arrResult['values'][0]['id'] : null;

		return $valReturn;
	}


	protected function createEmailonlyContact($strEmail)
	{
		$params = '{"values":{"display_name":"'.$strEmail.'","contact_type":"Individual"}}'; 
		$arrUrlParams = $this->buildUrl(["params"=>$params]);
		$arrResult = $this->writeToUrl('Contact/create', $arrUrlParams);
		$valID = ($arrResult['count']==1) ? $arrResult['values'][0]['id'] : null;

		if ($valID) {
			$params = '{"values":{"contact_id":'.$valID.', "email":"'.$strEmail.'"}}';
			$arrUrlParams = $this->buildUrl(['params'=>$params]);
			$arrResult = $this->writeToUrl('Email/create', $arrUrlParams);
		}

		return $valID;
	}


	protected function addCiviCRMID($intTl, $intCivicrm)
	{
		$db = Database::getInstance();
		$db->prepare("UPDATE tl_member SET civicrm_id=? WHERE id=?")
		   ->execute($intCivicrm, $intTl);
	}


	/*
	 * Groups
	 */

	protected function addToGroup($intUser, $intGroup)
	{
		$params = '{"values":{"group_id":'.$intGroup.',"contact_id":'.$intUser.'}}';
		$arrUrlParams = $this->buildUrl(["params"=>$params]);
		return $this->writeToUrl('GroupContact/create', $arrUrlParams);
	}


	protected function removeFromGroup($intUser, $intGroup)
	/* TODO: broken with API V4 */
	{
		$arrUrlParams = $this->buildUrl(array('contact_id'=>$intUser,
								        'group_id'=>$intGroup));

		return $this->writeToUrl('GroupContact/delete', $arrUrlParams);
	}

	/*
	* Participients
	*/

    private function getParticipant($arrSearch)
	/* TODO: broken with API V4 */
    {
      $arrUrlParams = $this->buildUrl($arrSearch);
      $arrReturn = $this->readFromUrl('Participant/get', $arrUrlParams);

      return $arrReturn;
    }


	/*
	 * Guzzle-Wrapper
	 */


	protected function buildUrl($arrParams = array())
	{
		$arrParams['key'] = $GLOBALS['TL_CONFIG']['civicrm_sitekey'];
		$arrParams['api_key'] = $GLOBALS['TL_CONFIG']['civicrm_apikey'];
		$arrParams['json'] = 1;

		$arrPairs = array();
		foreach ($arrParams as $name => $value)
		{
			$arrPairs[] = $name.'='.$value;
		}
		return $arrParams;
	}


	protected function readFromUrl($strAction, $arrUrlParams)
	{
		$data = $this->_callUrl($strAction, $arrUrlParams);

		if ($data == null) {
			return null;
		} else {
			return json_decode($data, true);
		}
	}


	protected function writeToUrl($strAction, $arrUrlParams)
	{
		$data = $this->_callUrl($strAction, $arrUrlParams);

		if ($data == null) {
			return null;
		} else {
			return json_decode($data, true);
		}
	}


	protected function _callUrl($strAction, $arrParams)
	{
		// print "\n<pre>";
		// print($GLOBALS['TL_CONFIG']['civicrm_apiurl']."\n");
		// print_r($strAction);
		// print("\n");
		// print_r($arrParams);
		// print "</pre>";

		$client = new Client([
			'base_uri' => $GLOBALS['TL_CONFIG']['civicrm_apiurl']
		]);

		try {
			$response = $client->request('POST', $strAction, ['query' => $arrParams]);
			return (string) $response->getBody();
		} catch (\GuzzleHttp\Exception\ServerException $e) {
			return null;
		}
		
		// print "\n<pre>";
		// $x = json_decode((string) $response->getBody());
		// print_r($x);
		// print "</pre>";

		
	}
}
