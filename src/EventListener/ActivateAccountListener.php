<?php

declare(strict_types=1);

namespace RodgauerWorkshop\ContaoCivicrmBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Module;
use Contao\MemberModel;
use RodgauerWorkshop\ContaoCivicrmBundle\CivicrmHelper;

/**
 * @Hook("activateAccount")
 */  
class ActivateAccountListener
{
	use CivicrmHelper;
	
    public function onActivateAccount(MemberModel $objUser, Module $module): void
	{
		$contact = $this->getContactByMail($objUser->email);

		if ($contact['count'] == 0) {
			/* no contact found -> create one and put him into members group */
			$valId = $this->createContact($objUser);
			$objUser->civicrm_id = $valId;
			if ($valId != null)
			{
				$this->addToGroup($valId, $GLOBALS['TL_CONFIG']['civicrm_membergroup']);
				$this->addCiviCRMID($objUser->id, $valId);
				// $this->updateNewsletter($objUser->newsletter, $objUser, true);
			}
		} elseif ($contact['count'] == 1) {
			/* exact one contact found, we will use it
			 * and add it to the members group */
			$objUser->civicrm_id = $contact['id'];
			$this->addToGroup($contact['id'],
							  $GLOBALS['TL_CONFIG']['civicrm_membergroup']);
			$this->addCiviCRMID($objUser->id, $contact['id']);
			// $this->updateNewsletter($objUser->newsletter, $objUser, true);
		} else {
			/* more then one contact found ... what now?
			 * error ... what now?
			 *
			 * Possible Solutions:
			 * Send me a mail, use first contact, do nothing
			 *
			 */
			// do nothing!
		}
	}
}
