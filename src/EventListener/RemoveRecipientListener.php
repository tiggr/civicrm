<?php

declare(strict_types=1);

namespace RodgauerWorkshop\ContaoCivicrmBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Database;
use RodgauerWorkshop\ContaoCivicrmBundle\CivicrmHelper;

/**
 * @Hook("removeRecipient")
 */  
class RemoveRecipientListener
{
	use CivicrmHelper;
	
    public function onRemoveRecipient($strEmail, $arrChannels)
	{
        $db = Database::getInstance();
        /* get userdata */
		$contact = $this->getContact(array('email'=>$strEmail));

		/* get group data */
		/* Only first group is used at the moment */
		$objChannels = $db->prepare("SELECT id, title, civicrm_group FROM tl_newsletter_channel WHERE id=?")
									   ->execute($arrChannels[0]);
		$arrGroups = $objChannels->fetchEach('civicrm_group');
		$intGroup = array_values($arrGroups)[0];

		if ($contact['is_error'] == 0 && $contact['count'] == 1) {
			/* contact found, we will remove it from the newsletter group */
			$this->removeFromGroup($contact['id'], $intGroup);
		}
	}
}
