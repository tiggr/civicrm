<?php

declare(strict_types=1);

namespace RodgauerWorkshop\ContaoCivicrmBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Database;
use RodgauerWorkshop\ContaoCivicrmBundle\CivicrmHelper;

/**
 * @Hook("activateRecipient")
 */  
class ActivateRecipientListener
{
	use CivicrmHelper;
	
	public function onActivateRecipient(string $strEmail, array $arrRecipients, array $arrChannels): void
	{
		$db = Database::getInstance();
		$valId = null;

		/* get userdata */
		$contact = $this->getContactByMail($strEmail);
		if ($contact['count'] != 0) { 
			$valId = $contact['values'][0]['contact_id'];
		}

		/* get group data */
		/* Only first group is used at the moment */
		$objChannels = $db->prepare("SELECT id, title, civicrm_group FROM tl_newsletter_channel WHERE id=?")->execute($arrChannels[0]);
		$arrGroups = $objChannels->fetchEach('civicrm_group');
		$intGroup = reset($arrGroups);

		/* Error values returned by CiviCRM are a bit inconsistent */
		if ($contact['count'] == 0) {
			/* no contact found -> create one and put him into newsletter group */
			$valId = $this->createEmailonlyContact($strEmail);

			if ($valId != null)
			{
				$this->addToGroup($valId, $intGroup);
			}
		} elseif ($contact['count'] == 1) {
			/* one contact found we will add it to the newsletter group */
			$this->addToGroup($valId, $intGroup);
		}
	}
}
