<?php

$GLOBALS['TL_LANG']['tl_content']['db_table'] = array('Database Table', 'In which table should the data be saved?');
$GLOBALS['TL_LANG']['tl_content']['workshop'] = array('Workshop', 'Workshops short name');
$GLOBALS['TL_LANG']['tl_content']['db_table_legend'] = 'Database';

$GLOBALS['TL_LANG']['tl_content']['output'] = array('Output', 'Output with wildcards using {{}}: first_name, last_name, display_name, sort_name, email');
$GLOBALS['TL_LANG']['tl_content']['mail'] = array('E-Mail', 'Send notification to...');
$GLOBALS['TL_LANG']['tl_content']['output_legend'] = 'Output inside frontend';
