<?php

$GLOBALS['TL_LANG']['tl_content']['db_table'] = array('Datenbanktabelle', 'Tabelle in der die Daten gespeichert werden');
$GLOBALS['TL_LANG']['tl_content']['workshop'] = array('Workshop', 'Kurzbezeichnung des Workshops');
$GLOBALS['TL_LANG']['tl_content']['db_table_legend'] = 'Datenbank';

$GLOBALS['TL_LANG']['tl_content']['output'] = array('Ausgabe', 'Ausgabe mit Platzhaltern in {{}}: first_name, last_name, display_name, sort_name, email');
$GLOBALS['TL_LANG']['tl_content']['mail'] = array('E-Mail', 'Benachrichtigung senden an...');
$GLOBALS['TL_LANG']['tl_content']['output_legend'] = 'Frontend-Ausgabe';
