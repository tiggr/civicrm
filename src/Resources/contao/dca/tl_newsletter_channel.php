<?php

declare(strict_types=1);

use RodgauerWorkshop\ContaoCivicrmBundle\EventListener\Listener\Hooks;

/**
 * Palettes
 */

$GLOBALS['TL_DCA']['tl_newsletter_channel']['palettes']['__selector__'][] = 'use_civicrm';
$GLOBALS['TL_DCA']['tl_newsletter_channel']['palettes']['default'] .= ';{civicrm_legend:hide},use_civicrm';
$GLOBALS['TL_DCA']['tl_newsletter_channel']['subpalettes']['use_civicrm'] = 'civicrm_group';

/**
 * Fields
 */

// $GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['newfield'] = array(
// 	'label' => 'Label',
// 	'inputType' => 'text',
// 	'default' => 'text'
// );

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['use_civicrm'] = array
(
	'label' => &$GLOBALS['TL_LANG']['tl_newsletter_channel']['use_civicrm'],
	'inputType'	=> 'checkbox',
	'eval' => array('submitOnChange'=>true),
	'sql' => "char(1) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['civicrm_group'] = array
(
	'label' => &$GLOBALS['TL_LANG']['tl_newsletter_channel']['civicrm_group'],
	'inputType'	=> 'select',
	'options_callback' => array(
		'RodgauerWorkshop\ContaoCivicrmBundle\CivicrmHelperClass', 
		'getAllGroups'
	),
	'default'	=> '',
	'eval' => array('mandatory'=>true, 'tl_class'=>'w50'),
	'sql' => "int(16) NULL default NULL"
);
