<?php

declare(strict_types=1);

use RodgauerWorkshop\ContaoCivicrmBundle\EventListener\Listener\Hooks;

$GLOBALS['TL_DCA']['tl_member']['palettes']['default'] .= ';{civicrm_legend:hidden},civicrm_id';

$GLOBALS['TL_DCA']['tl_member']['fields']['civicrm_id'] = array(
	'label' => &$GLOBALS['TL_LANG']['tl_member']['civicrm_id'],
	'inputType' => 'text',
	'eval' => array('disabled'=>true),
	'sql' => "int(10) NULL default NULL"
	);

$GLOBALS['TL_DCA']['tl_member']['fields']['newsletter']['save_callback'][] = array(
	'RodgauerWorkshop\ContaoCivicrmBundle\CivicrmHelperClass', 'updateNewsletter'
);
$GLOBALS['TL_DCA']['tl_member']['fields']['email']['save_callback'][] = array(
	'RodgauerWorkshop\ContaoCivicrmBundle\CivicrmHelperClass', 'updateEmail'
);
