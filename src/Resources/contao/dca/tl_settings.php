<?php

declare(strict_types=1);

use RodgauerWorkshop\ContaoCivicrmBundle\EventListener\Listener\Hooks;

/**
 * Add to palette
 */

$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] .= ';{civicrm_legend},civicrm_apiurl,civicrm_sitekey,civicrm_apikey,civicrm_membergroup';

/**
 * Add field
 */

$GLOBALS['TL_DCA']['tl_settings']['fields']['civicrm_apiurl'] = array(
	'label'		=> &$GLOBALS['TL_LANG']['tl_settings']['civicrm_apiurl'],
	'inputType'	=> 'text',
	'default'	=> 'http://localhost/',
	'eval'		=> array('mandatory'=>true, 'rgxp'=>'url', 'nospace'=>true, 'trailingSlash'=>true, 'tl_class'=>'long')
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['civicrm_sitekey'] = array(
	'label'		=> &$GLOBALS['TL_LANG']['tl_settings']['civicrm_sitekey'],
	'inputType'	=> 'text',
	'default'	=> '',
	'eval'		=> array('mandatory'=>true, 'rgxp'=>'extnd', 'nospace'=>true, 'minlength'=>7, 'maxlength'=>32, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['civicrm_apikey'] = array(
	'label'		=> &$GLOBALS['TL_LANG']['tl_settings']['civicrm_apikey'],
	'inputType'	=> 'text',
	'default'	=> '',
	'eval'		=> array('mandatory'=>true, 'rgxp'=>'extnd', 'nospace'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['civicrm_membergroup'] = array(
	'label'		=> &$GLOBALS['TL_LANG']['tl_settings']['civicrm_membergroup'],
	'inputType'	=> 'select',
	'options_callback' => array('RodgauerWorkshop\ContaoCivicrmBundle\CivicrmHelperClass', 'getAllGroups'),
	'default'	=> '',
	'eval'		=> array('mandatory'=>false, 'tl_class'=>'w50')
);
