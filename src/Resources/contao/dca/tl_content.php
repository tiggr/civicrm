<?php

$strName = 'tl_content';

$GLOBALS['TL_DCA']['tl_content']['palettes']['datawriter'] = '{type_legend},type,name,label;{db_table_legend},db_table,workshop;{output_legend},output,mail;{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['db_table'] = array(
    'label'     => $GLOBALS['TL_LANG']['tl_content']['db_table'],
    'inputType' => 'text',
    'eval'      => array(
        'mandatory' => true,
        'doNotSaveEmpty'=>true,
    ),
    'sql'=> 'varchar(256) null'
);

$GLOBALS['TL_DCA']['tl_content']['fields']['workshop'] = array(
    'label'     => $GLOBALS['TL_LANG']['tl_content']['workshop'],
    'inputType' => 'text',
    'eval'      => array(
        'mandatory' => true,
        'doNotSaveEmpty'=>true,
    ),
    'sql'=> 'varchar(256) null'
);

$GLOBALS['TL_DCA']['tl_content']['fields']['output'] = array(
    'label'     => $GLOBALS['TL_LANG']['tl_content']['output'],
    'inputType' => 'textarea',
    'default'   => '',
    'eval'      => array(
        'mandatory' => false,
        'doNotSaveEmpty'=>true,
        'rte' => 'tinyMCE',
    ),
    'sql' => "text NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['mail'] = array(
    'label'     => $GLOBALS['TL_LANG']['tl_content']['mail'],
    'inputType' => 'text',
    'eval'      => array(
        'mandatory' => true,
        'doNotSaveEmpty'=>true,
    ),
    'sql'=> 'varchar(256) null'
);
