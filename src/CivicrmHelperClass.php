<?php

declare(strict_types=1);

namespace RodgauerWorkshop\ContaoCivicrmBundle;


class CivicrmHelperClass
{
	use CivicrmHelper;
}